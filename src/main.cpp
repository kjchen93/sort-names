#include <iostream>
#include <string>
#include <fstream>
#include <vector>

/*
A code the takes a unsorted list of FIRST names in .txt format with new line for each name, 
and sorts the names in an alphabetical order using bubble sort.
*/
std::vector<std::string> bubbleSort(std::vector<std::string> vnames);


int main() {

    std::vector<std::string> names_unsorted;
    std::ifstream input("../names.txt");
    std::string newline;

    // reads file
    while (input >> newline) {
        names_unsorted.push_back(newline);
        }

    std::vector<std::string> names_sorted = bubbleSort(names_unsorted);

    //writes to file
    std::ofstream outfile;
    outfile.open("../names_sorted.txt");
    for (std::string name : names_sorted )
    
        outfile << name << "\n";

    outfile.close();
    
    
    return 0;
}


std::vector<std::string> bubbleSort(std::vector<std::string> vnames){
    int i = vnames.size() -1;

    while (i>=1)
    {
        int j = 0;
        while (j < i)
        {
            if (vnames[j] > vnames[j+1]){ // compares string alphabetically
                std::string dummy = vnames[j];
                vnames[j] = vnames[j+1];
                vnames[j+1] = dummy;
            }
            j+=1;
        }
        i -=1;
    }
    
    return vnames;

}