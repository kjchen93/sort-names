# Sort Names


## Description
A code the takes a unsorted list of FIRST names names.txt format with new line for each name, and sorts the names in an alphabetical order using bubble sort.
Outputs a file names_sorted.txt


## Example output

![](./sample.jpg)


## Usage
cd build

```bash
cd build
```
CMake
```bash
cmake ../
```
Make
```bash
make
```
run
```bash
./main
```

## Author
Kai Chen
